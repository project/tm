<?php

/**
 * @file
 *
 * Install, update and uninstall functions for the Email templates module.
 */

/**
 * Implements hook_install().
 */
function tm_email_install() {
  field_info_cache_clear();
  $fields = [];

  // Exported field: 'tm_template-email-field_tm_email_subject'.
  $fields['tm_template-email-field_tm_email_subject'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_tm_email_subject',
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => 0,
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'email',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'tm_template',
      'field_name' => 'field_tm_email_subject',
      'label' => 'Subject',
      'required' => 1,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 0,
      ),
    ),
  );

  // Exported field: 'tm_template-email-field_tm_email_body'.
  $fields['tm_template-email-field_tm_email_body'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_tm_email_body',
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'email',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'tm_template',
      'field_name' => 'field_tm_email_body',
      'label' => 'Email body',
      'required' => 1,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => 5,
        ),
        'type' => 'text_textarea',
        'weight' => 1,
      ),
    ),
  );

  // Included for use with string extractors like potx.
  st('Email body');
  st('Subject');

  foreach ($fields as $field) {
    // Create or update field.
    $field_config = $field['field_config'];
    if ($existing_field = field_info_field($field_config['field_name'])) {
      field_update_field($field_config);
    }
    else {
      field_create_field($field_config);
    }

    // Create or update field instance.
    $field_instance = $field['field_instance'];
    $existing_instance = field_info_instance($field_instance['entity_type'], $field_instance['field_name'], $field_instance['bundle']);
    if ($existing_instance) {
      field_update_instance($field_instance);
    }
    else {
      field_create_instance($field_instance);
    }
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * Implements hook_uninstall().
 */
function tm_email_uninstall() {
  field_delete_field('field_tm_email_subject');
  field_delete_field('field_tm_email_body');
}
