<?php

/**
 * @file
 * Hooks provided by the Email template manager module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide default templates.
 */
function hook_email_templates() {
  return [
    'email_register_no_approval_required' => [
      'label' => t('Welcome message when user self-registers'),
      'subject' => 'Default subject',
      'body' => 'Default body',
    ]
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
