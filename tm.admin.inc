<?php

/**
 * @file
 * Form builders and handlers.
 */

/**
 * Generates the Template type editing form.
 *
 * @param $form
 * @param $form_state
 * @param TemplateType $template_type
 * @param string $op
 *
 * @return array
 */
function tm_template_type_form($form, &$form_state, $template_type, $op = 'edit') {

  if ($op == 'clone') {
    $template_type->label .= ' (cloned)';
    $template_type->type = '';
  }

  $form['label'] = [
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $template_type->label,
    '#description' => t('The human-readable name of this Template type.'),
    '#required' => TRUE,
    '#size' => 30,
  ];

  // Machine-readable type name.
  $form['type'] = [
    '#type' => 'machine_name',
    '#default_value' => isset($template_type->type) ? $template_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $template_type->isLocked() && $op != 'clone',
    '#machine_name' => [
      'exists' => 'tm_template_types',
      'source' => ['label'],
    ],
    '#description' => t('A unique machine-readable name for this Template type. It must only contain lowercase letters, numbers, and underscores.'),
  ];

  if (module_exists('token')) {
    $form['settings'] = [
      '#title' => t('Settings'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    ];

    $types = [];
    $info = token_info();
    $global_types = token_get_global_token_types();
    foreach ($info['types'] as $type => $type_info) {
      // It's no need to select global types, they will available by default.
      if (in_array($type, $global_types)) {
        continue;
      }
      $types[$type] = $type_info['name'];
    }
    asort($types);

    $form['settings']['token_types'] = [
      '#title' => t('Supported token types'),
      '#description' => t('Selected token types with addition of global types will be available during management of template of this type.'),
      '#type' => 'checkboxes',
      '#options' => $types,
      '#default_value' => !empty($template_type->settings['token_types']) ? $template_type->settings['token_types'] : [],
    ];
  }

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  ];

  if (!$template_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 45,
      '#limit_validation_errors' => [],
      '#submit' => ['tm_template_type_form_delete_submit']
    ];
  }

  return $form;
}

/**
 * Submit handler for creating/editing tm_template_type.
 *
 * @param $form
 * @param $form_state
 */
function tm_template_type_form_submit(&$form, &$form_state) {
  $template_type = entity_ui_form_submit_build_entity($form, $form_state);

  // Wipe zero values as they are useless.
  if (isset($template_type->settings['token_types'])) {
    $template_type->settings['token_types'] = array_filter($template_type->settings['token_types']);
  }

  // Save and go back.
  tm_template_type_save($template_type);

  // Redirect user back to list of template types.
  $form_state['redirect'] = 'admin/structure/template-types';
}

/**
 * Submit handler for delete button.
 *
 * @param $form
 * @param $form_state
 */
function tm_template_type_form_delete_submit(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/template-types/manage/' . $form_state['tm_template_type']->type . '/delete';
}

/**
 * Page to select Template Type to add new Template.
 *
 * @return array
 */
function tm_template_admin_add_page() {
  $types = [];
  foreach (tm_template_types() as $name => $type) {
    $types[$name] = entity_label('tm_template_type', $type);
  }

  if (!empty($types)) {
    // Extra work to show types ordered by label.
    asort($types);
    $items = [];
    foreach ($types as $name => $label) {
      $items[] = l($label, 'admin/content/templates/add/' . $name);
    }

    $output = [
      'list' => [
        '#theme' => 'item_list',
        '#items' => $items,
        '#title' => t('Select type of template to create')
      ]
    ];
  }
  else {
    if (user_access('administer template types')) {
      $message = t(
        'Templates cannot be added because you have not created any Template types yet. Go to the <a href="@create-type">Template type creation page</a> to add one.',
        ['@create-type' => url('admin/structure/template-types/add')]
      );
    }
    else {
      $message = t('No Template types have been created yet.');
    }
    $output = [
      '#markup' => '<p>' . $message . '</p>',
    ];
  }

  return $output;
}

/**
 * Template Form.
 *
 * @param $form
 * @param $form_state
 * @param Template $template
 *
 * @return array
 */
function tm_template_form($form, &$form_state, Template $template) {
  $form_state['tm_template'] = $template;

  $form['label'] = [
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $template->label,
    '#weight' => -100,
  ];

  $entity_id = entity_id('tm_template', $template);

  // We add bundle name as a prefix of machine name to make it fully-unique.
  $field_prefix = $template->type . '_';
  $form['name'] = [
    '#type' => 'machine_name',
    '#field_prefix' => $field_prefix,
    // If name already set, remove type prefix - it will be added in submit.
    '#default_value' => isset($template->name) ? str_replace($field_prefix, '', $template->name) : '',
    '#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#size' => '64',
    '#required' => TRUE,
    '#machine_name' => [
      'exists' => 'tm_template_name_exists',
      'source' => ['label'],
      'replace_pattern' => '[^0-9a-z_\-]',
      'error' => t('Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for style names.'),
    ],
    // Disallow edition of template machine name to avoid complex validation logic.
    '#disabled' => $entity_id,
  ];

  // Prepare field for proper language.
  $langcode = function_exists('entity_language') ? entity_language('tm_template', $template) : NULL;
  field_attach_form('tm_template', $template, $form, $form_state, $langcode);

  if (module_exists('token')) {
    $template_type = tm_template_type_load($template->type);
    $types = isset($template_type->settings['token_types']) ? array_keys($template_type->settings['token_types']) : [];
    $form['tokens'] = [
      '#title' => t('Token replacements'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 90,
    ];
    $form['tokens']['token_tree'] = [
      '#theme' => 'token_tree',
      '#token_types' => $types,
      '#show_restricted' => TRUE,
    ];
  }

  $form['actions'] = [
    '#weight' => 100,
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  // Show Delete button if we edit template and it's not in code.
  if (!$template->isLocked() && $entity_id && tm_template_access('delete', $template)) {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => ['tm_template_form_delete_submit'],
    ];
  }

  return $form;
}

/**
 * Template submit handler.
 *
 * @param $form
 * @param $form_state
 */
function tm_template_form_submit($form, &$form_state) {
  $template = $form_state['tm_template'];

  entity_form_submit_build_entity('tm_template', $template, $form, $form_state);

  // Add type prefix back to name.
  $template->name = $template->type . '_' . $template->name;

  tm_template_save($template);

  $form_state['redirect'] = 'admin/content/templates';

  drupal_set_message(t('Template %title saved.', ['%title' => entity_label('tm_template', $template)]));
}

/**
 * Template delete form submit handler.
 *
 * @param $form
 * @param $form_state
 */
function tm_template_form_delete_submit($form, &$form_state) {
  /** @var Template $template */
  $template = $form_state['tm_template'];
  $form_state['redirect'] = [
    'admin/content/templates/manage/' . $template->name  . '/delete',
    ['query' => ['destination' => 'admin/content/templates']]
  ];
}
