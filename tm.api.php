<?php

/**
 * @file
 * Template manager hook examples.
 */

/**
 * Returns info about template types.
 *
 * @return array
 *   Description of available template types.
 */
function hook_default_tm_template_type() {
  $items = [];

  $items['email'] = entity_import(
    'tm_template_type',
    '{ "type" : "email", "label" : "Email", "settings" : { "token_types" : { "user" : "user", "node" : "node" } } }'
  );

  return $items;
}

/**
 * Alter default template types.
 *
 * If you want to modify some template type provided by any submodule of tm
 * use this hook.
 *
 * @param array $defaults
 *   Default template types to alter.
 */
function hook_default_tm_template_type_alter(array &$defaults) {
  $defaults['email']->settings['token_types']['comment'] = 'comment';
}
