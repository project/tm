<?php

/**
 * @file
 * Bean translation handler for the translation module.
 */

/**
 * Bean translation handler.
 *
 * @TODO: There is a problem with tabs in localTasksAlter(). There is no depth by key. And there is a notice about it.
 *
 * Overrides default behaviours for Bean properties.
 */
class EntityTranslationTemplateHandler extends EntityTranslationDefaultHandler {

  /**
   * {@inheritdoc}
   */
  protected function getEntityId() {
    // We need to override getEntityId() to use entity_id instead of entity_extract_ids().
    return entity_id('tm_template', $this->entity);
  }

}
