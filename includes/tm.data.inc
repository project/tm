<?php

/**
 * @file
 * Rules integration for the Template Manager module.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Custom replacement of RulesDataUIText to extend form and add templates selector.
 */
class RulesDataUITemplate extends RulesDataUIText {

  /**
   * {@inheritdoc}
   */
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = parent::inputForm($name, $info, $settings, $element);

    // Don't affect on select list params.
    if (!empty($info['options list'])) {
      return $form;
    }

    $template_enabled = isset($settings[$name . ':templates']['enabled'])
      ? $settings[$name . ':templates']['enabled']
      : FALSE;
    $form[$name . ':templates'] = [
      '#type' => 'fieldset',
      '#title' => t('Templates'),
      '#collapsible' => TRUE,
      '#collapsed' => !$template_enabled,
      '#tree' => TRUE,
    ];
    $form[$name . ':templates']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Use template'),
      '#default_value' => $template_enabled,
      '#required' => FALSE,
    ];

    $options = [];
    $types = tm_template_type_names();
    $templates = tm_template_load_multiple(FALSE, []);
    foreach ($templates as $template) {
      $type_name = $types[$template->type];
      if (!isset($options[$type_name])) {
        $options[$type_name] = [];
      }
      $options[$type_name][$template->name] = $template->label;
    }

    $default_template = isset($settings[$name . ':templates']['template'])
      ? $settings[$name . ':templates']['template']
      : FALSE;
    $enabled_selector = ':input[name="parameter[' . $name . '][settings][' . $name . ':templates][enabled]"]';
    $form[$name . ':templates']['template'] = [
      '#type' => 'select',
      '#title' => t('Template'),
      '#options' => $options,
      '#default_value' => $default_template,
      '#required' => TRUE,
      '#states' => [
        'visible' => [$enabled_selector => ['checked' => TRUE]],
        'required' => [$enabled_selector => ['checked' => TRUE]],
      ],
    ];

    // Original field is a opposite of template.
    // If template is enabled, de-activate regular param form.
    $form[$name]['#states'] = [
      'invisible' => [$enabled_selector => ['checked' => TRUE]],
      'required' => [$enabled_selector => ['unchecked' => TRUE]],
    ];

    // Add custom validation to fix validate before submit.
    $form[$name]['#element_validate'][] = 'tm_template_rules_param_validate';
    // Also we need to remove require flag to make possible submit empty value
    // until our element validate is invoked.
    $form[$name]['#required'] = FALSE;
    $form[$name]['#param_name'] = $name;
    // Wipe default value of param which is actually fake value from element validate.
    // It's needed to show empty param name if template will be displayed to avoid confusion.
    $form[$name]['#default_value'] = FALSE;

    return $form;
  }

}

/**
 * @}
 */
