<?php

/**
 * @file
 *
 * Contains rules integration for the Template Manager module needed during evaluation.
 *
 * @addtogroup rules
 * @{
 */

class RulesTemplateEvaluator extends RulesTokenEvaluator {

  /**
   * {@inheritdoc}
   */
  public static function access() {
    return user_access('administer template entities');
  }

  /**
   * {@inheritdoc}
   */
  public function prepare($text, $var_info) {
    // A returned NULL skips the evaluator.
    $this->setting = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function process($value, $info, RulesState $state, RulesPlugin $element, $options = NULL) {
    $options = isset($options) ? $options : $this->getEvaluatorOptions($info, $state, $element);
    $value = isset($this->processor) ? $this->processor->process($value, $info, $state, $element, $options) : $value;
    $name = $info['#name'];
    if (!empty($element->settings[$name . ':templates']['enabled'])) {
      $template = tm_template_load_by_name($element->settings[$name . ':templates']['template']);
      // @TODO: Remove hard-code and make usable for any template type.
      // @TODO: Add support of multi-lingual pushes and fields. See tm_email_mail_alter() as example.
      $value = $template->field_tm_push_message[LANGUAGE_NONE][0]['value'];
      // We need to force token processing for case when TokenEvaluator is not attached.
      // It's not a problem to run it twice - after first replace, nothing to replace again.
      $value = parent::evaluate($value, $options, $state);
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate($text, $options, RulesState $state) {
    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public static function help($var_info) {
    // Don't return any description – all will be specified in extra param section.
    return FALSE;
  }

}

/**
 * @}
 */
