# TODOs

* Try to store translation settings.
* Check menu and URL structure.
* Check defaultUri and URI generation for entity. Similar problem for translation overview page.
* Consider usage of hook_entity_property_info_alter().
* Categories of templates.
* In-file storage (tpl?).
* Add views integration for main templates list.
* Fix page title for templates list.
