<?php

/**
 * @file
 * Rules integration for the Template Manager module.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_evaluator_info().
 */
function tm_rules_evaluator_info() {
  return [
    'template' => [
      'class' => 'RulesTemplateEvaluator',
      'type' => ['text'],
      'weight' => -100,
    ],
  ];
}

/**
 * Implements hook_rules_data_info().
 *
 * @param $data_info
 */
function tm_rules_data_info_alter(&$data_info) {
  // Replace handler for text type with custom Template class.
  $data_info['text']['ui class'] = 'RulesDataUITemplate';
}
